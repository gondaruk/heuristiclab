(function() {
  'use strict';

  angular.module('app', [
    // angular
    'ui.router',
    'ngAnimate',

    // foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ])
    .config(config)
    .run(run)

    .factory('ServerCall', ServerCall)

    .controller('HomepageCtrl', HomepageCtrl)

    .controller('SettingsCriterionsCtrl', SettingsCriterionsCtrl)
    .controller('SettingsProfessionsCtrl', SettingsProfessionsCtrl)
    .controller('SettingsCandidatesCtrl', SettingsCandidatesCtrl)

    .controller('AnalyzeCandidatesCtrl', AnalyzeCandidatesCtrl)
    .controller('AnalyzeProfessionsCtrl', AnalyzeProfessionsCtrl)
    .controller('AnalyzeMatchesCtrl', AnalyzeMatchesCtrl)

  ;









  // #########################
  // Factories

  ServerCall.$inject = ['$http'];
  function ServerCall($http){
    var urlBase = '/api/';
    var cans = {};

    cans.getCriterions = function(success_callback){
      var url = urlBase + "config/criterions/get";
      $http.post(url).success(success_callback);
    };

    cans.saveCriterions = function(data, success_callback){
      var url = urlBase + "config/criterions/save";
      $http.post(url, {data:data}).success(success_callback);
    };



    cans.getProfessions = function(success_callback){
      var url = urlBase + "config/professions/get";
      $http.post(url).success(success_callback);
    };

    cans.saveProfessions = function(data, success_callback){
      var url = urlBase + "config/professions/save";
      $http.post(url, {data:data}).success(success_callback);
    };



    cans.getCandidates = function(success_callback){
      var url = urlBase + "config/candidates/get";
      $http.post(url).success(success_callback);
    };

    cans.saveCandidates = function(data, success_callback){
      var url = urlBase + "config/candidates/save";
      $http.post(url, {data:data}).success(success_callback);
    };



    cans.getCandidatesProfessions = function(success_callback){
      var url = urlBase + "find/candidates_professions";
      $http.post(url).success(success_callback);
    };

    cans.analyzeCandidate = function(data, success_callback){
      var url = urlBase + "analyze/candidate";
      $http.post(url, {data:data}).success(success_callback);
    };

    cans.analyzeProfession = function(data, success_callback){
      var url = urlBase + "analyze/profession";
      $http.post(url, {data:data}).success(success_callback);
    };

    cans.analyzeMatches = function(data, success_callback){
      var url = urlBase + "analyze/matches";
      $http.post(url, {data:data}).success(success_callback);
    };


    return cans;
  };









  // #########################
  // Controllers


// start of HomepageCtrl
HomepageCtrl.$inject = ['$controller', '$scope', '$rootScope'];
function HomepageCtrl($controller, $scope, $rootScope) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, $rootScope: $rootScope}));
    $scope.keyChanged = function () {
        localStorage.setItem("key", $rootScope.data.key);
    };
}; // end of HomepageCtrl


  // start of SettingsCriterionsCtrl
  SettingsCriterionsCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function SettingsCriterionsCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));

    ServerCall.getCriterions(function(data){
      $scope.data = data;
    });

    $scope.resetChanges = function(){
      ServerCall.getCriterions(function(data){
        $scope.data = data;
        FoundationApi.publish('main-notification', { color:"alert", title: 'Изменение отклонены!' });
      });
    };

    $scope.saveChanges = function(){
      ServerCall.saveCriterions($scope.data.data, function(data){
        ServerCall.getCriterions(function(data){
          $scope.data = data;
          FoundationApi.publish('main-notification', { color:"success", title: 'Сохранено!' });
        });
      });
    };



    $scope.addCriterion = function(){
      $scope.data.data.push(
        {
          "ignore": false,
          "question_text": "Новый вопрос...",
          "answers": [
              { "answer_text":"Ні" },
              { "answer_text":"Так" }
            // { "answer_text":"Взагалі не знаю" },
            // { "answer_text":"Погано" },
            // { "answer_text":"На середньому рівні" },
            // { "answer_text":"Досить добре" },
            // { "answer_text":"Відмінно" },
            // { "answer_text":"Ідеально" }
          ]
        }
      );

    };

    $scope.addAnswerToCriterion = function(q){
      q.answers.push(
        {
          "answer_text":""
        }
      );
    };

  }; // end of SettingsCriterionsCtrl
































  // start of SettingsProfessionsCtrl
  SettingsProfessionsCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function SettingsProfessionsCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));

    ServerCall.getProfessions(function(data){
      $scope.data = data;
    });

    $scope.resetChanges = function(){
      ServerCall.getProfessions(function(data){
        $scope.data = data;
        FoundationApi.publish('main-notification', { color:"alert", title: 'Изменение отклонены!' });
      });
    };

    $scope.saveChanges = function(){
      ServerCall.saveProfessions($scope.data.data.professions, function(data){
        ServerCall.getProfessions(function(data){
          $scope.data = data;
          FoundationApi.publish('main-notification', { color:"success", title: 'Сохранено!' });
        });
      });
    };

    $scope.addProfession = function(){
      $scope.data.data.professions.push({
        "profession_name":"Новая профессия...",
        "weight":{}
      });
    };
  }; // end of SettingsProfessionsCtrl





















  // start of SettingsCandidatesCtrl
  SettingsCandidatesCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function SettingsCandidatesCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));

    ServerCall.getCandidates(function(data){
      $scope.data = data;
    });

    $scope.resetChanges = function(){
      ServerCall.getCandidates(function(data){
        $scope.data = data;
        FoundationApi.publish('main-notification', { color:"alert", title: 'Изменение отклонены!' });
      });
    };

    $scope.saveChanges = function(){
      var critLen = $scope.data.data.criterions.length;
      var allOk = true;
      $scope.data.data.candidates.forEach(function(candidate) {
        if (candidate.info.getLength() != critLen){
          FoundationApi.publish('main-notification', { color:"alert", title: 'Вы не заполнили все ответы!', content:"Пользователь: "+candidate.candidate_name });
          allOk = false;
          return 0;
        }
      });

      if (allOk){
        ServerCall.saveCandidates($scope.data.data.candidates, function(data){
          ServerCall.getCandidates(function(data){
            $scope.data = data;
            FoundationApi.publish('main-notification', { color:"success", title: 'Сохранено!' });
          });
        });
      };
    };


    $scope.addCandidate = function(){
      $scope.data.data.candidates.push({
        "candidate_name":"Новая акмеограмма...",
        "info":{}
      });
    }

  }; // end of SettingsCandidatesCtrl


























  // start of AnalyzeCandidatesCtrl
  AnalyzeCandidatesCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function AnalyzeCandidatesCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));
    ServerCall.getCandidatesProfessions(function(data){
      $scope.data = data;
      $scope.data.prerequest = {};
      $scope.data.prerequest.professions = {};
      $scope.checkAll();
    });

    $scope.checkAll = function(){
      $scope.data.data.professions.forEach(function(pro){
        $scope.data.prerequest.professions[pro._id.$oid] = true;
      });
    };

    $scope.uncheckAll = function(){
      $scope.data.data.professions.forEach(function(pro){
        $scope.data.prerequest.professions[pro._id.$oid] = false;
      });
    };

    $scope.startAnalyze = function(){
      if (!$scope.data.prerequest.candidate_id){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите акмеограмму!' });
        return 0;
      }
      if (!$scope.data.prerequest.professions.haveAtLeastOneTrue()){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите хоть одну профессиограмму!' });
        return 0;
      }

      ServerCall.analyzeCandidate($scope.data.prerequest, function(data){
        $scope.data.results = data.data;
      });
    };
  }; // end of AnalyzeCandidatesCtrl




















  // start of AnalyzeProfessionsCtrl
  AnalyzeProfessionsCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function AnalyzeProfessionsCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));
    ServerCall.getCandidatesProfessions(function(data){
      $scope.data = data;
      $scope.data.prerequest = {};
      $scope.data.prerequest.candidates = {};
      $scope.checkAll();
    });

    $scope.checkAll = function(){
      $scope.data.data.candidates.forEach(function(pro){
        $scope.data.prerequest.candidates[pro._id.$oid] = true;
      });
    };

    $scope.uncheckAll = function(){
      $scope.data.data.candidates.forEach(function(pro){
        $scope.data.prerequest.candidates[pro._id.$oid] = false;
      });
    };

    $scope.startAnalyze = function(){
      if (!$scope.data.prerequest.profession_id){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите профессию!' });
        return 0;
      }
      if (!$scope.data.prerequest.candidates.haveAtLeastOneTrue()){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите хоть одну акмеограмму!' });
        return 0;
      }

      ServerCall.analyzeProfession($scope.data.prerequest, function(data){
        $scope.data.results = data.data;
      });
    };
  }; // end of AnalyzeProfessionsCtrl


















  // start of AnalyzeMatchesCtrl
  AnalyzeMatchesCtrl.$inject = ['$controller', '$scope', 'FoundationApi', 'ServerCall'];
  function AnalyzeMatchesCtrl($controller, $scope, FoundationApi, ServerCall) {
    angular.extend(this, $controller('DefaultController', {$scope: $scope, FoundationApi:FoundationApi, ServerCall: ServerCall}));
    ServerCall.getCandidatesProfessions(function(data){
      $scope.data = data;
      $scope.data.prerequest = {};
      $scope.data.prerequest.candidates = {};
      $scope.data.prerequest.professions = {};
      $scope.checkAllCans();
      $scope.checkAllProfs();
    });

    $scope.checkAllCans = function(){
      $scope.data.data.candidates.forEach(function(can){
        $scope.data.prerequest.candidates[can._id.$oid] = true;
      });
    };

    $scope.uncheckAllCans = function(){
      $scope.data.data.candidates.forEach(function(can){
        $scope.data.prerequest.candidates[can._id.$oid] = false;
      });
    };

    $scope.checkAllProfs = function(){
      $scope.data.data.professions.forEach(function(pro){
        $scope.data.prerequest.professions[pro._id.$oid] = true;
      });
    };

    $scope.uncheckAllProfs = function(){
      $scope.data.data.professions.forEach(function(pro){
        $scope.data.prerequest.professions[pro._id.$oid] = false;
      });
    };

    $scope.startAnalyze = function(){

      if (!$scope.data.prerequest.candidates.haveAtLeastOneTrue()){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите хоть одну акмеограмму!' });
        return 0;
      }

      if (!$scope.data.prerequest.professions.haveAtLeastOneTrue()){
        FoundationApi.publish('main-notification', { color:"alert", title: 'Выберите хоть одну профессиограмму!' });
        return 0;
      }

      ServerCall.analyzeMatches($scope.data.prerequest, function(data){
        $scope.data.results = data.data;
      });
    };
  }; // end of AnalyzeMatchesCtrl




















  // Starting
  config.$inject = ['$urlRouterProvider', '$locationProvider'];
  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  };

  function run($rootScope) {
    $rootScope.data = {};
    $rootScope.data.trueKey = "admin";
    if (localStorage.getItem("key")){
        $rootScope.data.key = localStorage.getItem("key");
    }
    Object.prototype.getLength = function(){ return Object.keys(this).length; };
    Object.prototype.haveAtLeastOneTrue = function(){
      for (var key in this) {
        if (this[key]==true){
          return true;
        }
      }
      return false;
    };
    FastClick.attach(document.body);
  };

})();
