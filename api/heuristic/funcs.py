##############################
# MODULE 1 - One To Many
def OneToMany(candidate, professions):
    res = {}
    res['candidate_name'] = candidate['candidate_name']
    res['professions'] = []
    for prof in professions:
        newProfession = {}
        newProfession['profession_id'] = str(prof['_id'])
        newProfession['profession_name'] = prof['profession_name']
        newProfession['compatibility_percentage'] = 0
        newProfession['compatibility_sum'] = 0
        newProfession['compatibility_sum_max'] = 0
        for crit in candidate['info']:
            # Sums every answer of user to the 'compatibility_sum'
            newProfession['compatibility_sum'] += int(prof['weight'][crit][str(candidate['info'][crit])])
            # From every criterion selects an answer with maximum mark and it sums to the 'compatibility_sum_max'
            newProfession['compatibility_sum_max'] += max(int(prof['weight'][crit][str(r)]) for r in prof['weight'][crit])
        # Calculating percentage
        if newProfession['compatibility_sum_max'] != 0 and newProfession['compatibility_sum'] > 0:
            newProfession['compatibility_percentage'] = newProfession['compatibility_sum']/float(newProfession['compatibility_sum_max'])
        res['professions'].append(newProfession)
    return res










##############################
# MODULE 2 - Many To One
def ManyToOne(profession, candidates):
    res = {}
    res['profession_name'] = profession['profession_name']
    res['candidates'] = []
    for cand in candidates:
        newCandidate = {}
        newCandidate['candidate_id'] = str(cand['_id'])
        newCandidate['candidate_name'] = cand['candidate_name']
        newCandidate['compatibility_percentage'] = 0
        newCandidate['compatibility_sum'] = 0
        newCandidate['compatibility_sum_max'] = 0
        for crit in cand['info']:
            # Sums every answer of user to the 'compatibility_sum'
            newCandidate['compatibility_sum'] += int(profession['weight'][crit][str(cand['info'][crit])])
            # From every criterion selects an answer with maximum mark and it sums to the 'compatibility_sum_max'
            newCandidate['compatibility_sum_max'] += max(int(profession['weight'][crit][str(r)]) for r in profession['weight'][crit])
        # Calculating percentage
        if newCandidate['compatibility_sum_max'] != 0 and newCandidate['compatibility_sum'] > 0:
            newCandidate['compatibility_percentage'] = newCandidate['compatibility_sum']/float(newCandidate['compatibility_sum_max'])
        res['candidates'].append(newCandidate)
    return res










##############################
# MODULE 3 - Many To Many
def ManyToMany(professions, candidates):
    res = {}
    for cand in candidates:
        res[str(cand['_id'])] = {}
        res[str(cand['_id'])]['candidate_id'] = str(cand['_id'])
        res[str(cand['_id'])]['candidate_name'] = cand['candidate_name']
        res[str(cand['_id'])]['professions'] = []
        for prof in professions:
            newMatch = {}
            newMatch['profession_id'] = str(prof['_id'])
            newMatch['profession_name'] = prof['profession_name']
            newMatch['compatibility_percentage'] = 0
            newMatch['compatibility_sum'] = 0
            newMatch['compatibility_sum_max'] = 0
            for crit in cand['info']:
                # Sums every answer of user to the 'compatibility_sum'
                newMatch['compatibility_sum'] += int(prof['weight'][crit][str(cand['info'][crit])])
                # From every criterion selects an answer with maximum mark and it sums to the 'compatibility_sum_max'
                newMatch['compatibility_sum_max'] += max(int(prof['weight'][crit][str(r)]) for r in prof['weight'][crit])
            # Calculating percentage
            if newMatch['compatibility_sum_max'] != 0 and newMatch['compatibility_sum'] > 0:
                newMatch['compatibility_percentage'] = newMatch['compatibility_sum']/float(newMatch['compatibility_sum_max'])
            res[str(cand['_id'])]['professions'].append(newMatch)
    return res
    



























### END OF FILE ###
