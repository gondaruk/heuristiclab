#!/usr/local/bin/python

from bottle import run, hook, response, request, route, get, post
import pymongo
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
import heuristic




##############################
# Hook for CORS
@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
    response.headers['Access-Control-Allow-Headers'] = 'x-prototype-version,x-requested-with'
    response.headers['Access-Control-Allow-Credentials'] = 'true'


##############################
# Database connection and collections initialization
db = pymongo.MongoClient("mongodb://127.0.0.1").heuristiclab
db_criterions  = db.criterions
db_professions = db.professions
db_candidates  = db.candidates

##############################
# Config criterions
@route('/api/config/criterions/get', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Return all criterions
        No data
    '''
    dout = {}
    try:
        dout['status'] = 'success'
        dout['data'] = db_criterions.find()
    except:
        dout['status'] = 'error'
    return dumps(dout)

@route('/api/config/criterions/save', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Saving all criterions
        raw array in body
    '''
    dout = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        for doc in din['data']:
            db_criterions.save(doc)
    except:
        dout['status'] = 'error'
    return dumps(dout)



##############################
# Config professions
@route('/api/config/professions/get', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Return all professions and criterions
        No arguments
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        dout['data']['criterions'] = db_criterions.find()
        dout['data']['professions'] = db_professions.find()
    except:
        dout['status'] = 'error'
    return dumps(dout)

@route('/api/config/professions/save', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Saving all professions
        raw array in body
    '''
    dout = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        for doc in din['data']:
            db_professions.save(doc)
    except:
        dout['status'] = 'error'
    return dumps(dout)




##############################
# Config candidates
@route('/api/config/candidates/get', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Return all candidates and criterions
        No arguments
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        dout['data']['criterions'] = db_criterions.find()
        dout['data']['candidates'] = db_candidates.find()
    except:
        dout['status'] = 'error'
    return dumps(dout)

@route('/api/config/candidates/save', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Saving all candidates
        raw array in body
    '''
    dout = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        for doc in din['data']:
            db_candidates.save(doc)
    except:
        dout['status'] = 'error'
    return dumps(dout)



##############################
# Selections


@route('/api/find/candidates_professions', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Return all candidates and professions
        No arguments
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        dout['data']['candidates'] = db_candidates.find()
        dout['data']['professions'] = db_professions.find()
    except:
        dout['status'] = 'error'
    return dumps(dout)









##############################
# Analyzing
@route('/api/analyze/candidate', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Analyze ONE candidate
        raw array in body
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        profList = []
        for (prof, need) in din['data']['professions'].items():
            if need:
                profList.append(ObjectId(prof))
        candidate = db_candidates.find_one({'_id':ObjectId(din['data']['candidate_id'])})
        professions = list(db_professions.find({'_id': {'$in': profList}}))
        dout['data'] = heuristic.OneToMany(candidate, professions)
    except:
        dout['status'] = 'error'
    return dumps(dout)

@route('/api/analyze/profession', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Analyze ONE profession
        raw array in body
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        cansList = []
        for (can, need) in din['data']['candidates'].items():
            if need:
                cansList.append(ObjectId(can))
        profession = db_professions.find_one({'_id':ObjectId(din['data']['profession_id'])})
        candidates = list(db_candidates.find({'_id': {'$in': cansList}}))
        dout['data'] = heuristic.ManyToOne(profession, candidates)
    except:
        dout['status'] = 'error'
    return dumps(dout)

@route('/api/analyze/matches', method=['POST', 'OPTIONS'])
def api_route():
    '''
        Analyze the best candidate for every profession
        raw array in body
    '''
    dout = {}
    dout['data'] = {}
    try:
        dout['status'] = 'success'
        din = loads(request.body.read())
        cansList = []
        profList = []
        for (can, need) in din['data']['candidates'].items():
            if need:
                cansList.append(ObjectId(can))
        for (prof, need) in din['data']['professions'].items():
            if need:
                profList.append(ObjectId(prof))
        candidates = list(db_candidates.find({'_id': {'$in': cansList}}))
        professions = list(db_professions.find({'_id': {'$in': profList}}))
        dout['data'] = heuristic.ManyToMany(professions, candidates)
    except:
        dout['status'] = 'error'
    return dumps(dout)









##############################
# debug
@route('/api/debug', method=['POST', 'OPTIONS'])
def api_route():
    return "nope"












run(server='paste', host='0.0.0.0', port=82, reloader=True, debug=True)
